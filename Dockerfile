FROM python:3.9
  
WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# Copy source code to /app directory in the container
COPY . /app

ENV FLASK_APP=handle_str.py

EXPOSE 5000

CMD ["python", "-m", "flask", "run", "-h", "0.0.0.0"]

