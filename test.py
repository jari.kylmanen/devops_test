import analyze_chars as anal1
import handle_str as anal2
import time as _time

str = 'Testi 123-a(Merkkijonotesti)'

def analyze1(str):

    ts1 = _time.time()*1000
    d = anal1.calculate_digits(str)
    l = anal1.calculate_letters(str)
    s = anal1.calculate_spaces(str)

    rest = len(str)-d-l-s
    dur1 = _time.time()*1000-ts1
    return {'length':len(str),'digits':d,'letters':l,'spaces':s,'oteher':rest,'duration':dur1}

def analyze2(str):

    ts2 = _time.time()
    data2 = anal2.analyze_string(str)
    dur2 = _time.time()-ts2
    return data2

print('Reg-expr: %s' %(analyze1(str)))
print('For-loop: %s' %(analyze2(str)))

