from flask import Flask, request, render_template
import time as _time
import json
import socket
from os import environ
import redis

app = Flask(__name__)

@app.route('/')
def analyzer_form():
    incrCounter()
    return render_template('text-analyzer.html', 
        ret_msg=json.dumps({'Visitor count':getCounter()}))

@app.route('/', methods=['POST'])
def analyzer_form_post():
    user_input = request.form['user-input']
    ret = analyze_string(user_input)
    ret['visits'] = getCounter()
    #ret['visit_count'] = getLastSeen()
    #setLastSeen()

    return render_template('text-analyzer.html',ret_msg=json.dumps(ret))

def getRedis():
    return redis.Redis(
        host=environ.get("REDIS_HOST", "localhost"),
        port=int(environ.get("REDIS_PORT", "6379"))
    )

def incrCounter():
    r = getRedis()
    r.incr("counter", 1)

def getCounter():
    r = getRedis()
    #r.incr("counter", 1)
    return r.get("counter").decode('utf-8')

def getLastSeen():
    r = getRedis()
    s = r.get("lastseen")
    return "Last seen %s" %(s.decode('utf-8')) if s else ''

def setLastSeen():
    r = getRedis()
    return r.set("lastseen",_time.ctime())


def get_length(input_str):
    return len(input_str)

def get_char_count(input_str):
    nums = 0
    alphs = 0
    spaces = 0
    others = 0

    for character in input_str:
        if character.isdigit():
            nums = nums+1
        elif character.isalpha():
            alphs = alphs+1
        elif character == ' ':
            spaces = spaces+1
        else:
            others = others+1

    return {
        'numbers'   : nums,
        'alphabets' : alphs,
        'spaces'    : spaces,
        'others'    : others,
    }

def analyze_string(input_str):
    begin_ts = _time.time()*1000

    data = get_char_count(input_str)
    data['length'] = len(input_str)
    data['duration'] = "%f ms" %(_time.time()*1000 - begin_ts)
    return data

#a=analyze_string('abc 3-3')
#print (a)


