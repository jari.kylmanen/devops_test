import re

integer_pattern = re.compile('[\D_]')
str_pattern = re.compile('[^a-zA-Z]')
space_pattern = re.compile('[^ ]')

def calculate_digits(chars):
    return len(integer_pattern.sub('', chars))

def calculate_letters(chars):
    return len(unicode(str_pattern.sub('', chars), encoding='utf8'))

def calculate_spaces(chars):
    return len(space_pattern.sub('', chars))

